class ApplicationInteractor
  include ActiveModel::Validations

  class << self
    def call(context = {})
      new(context).tap do |command|
        command.validate && command.execute.tap do |result|
          command.result = result
        end
      end
    end
  end

  def initialize(context = {})
    @context = context.to_h.with_indifferent_access
  end

  def result
    context[:result]
  end

  def result=(value)
    context[:result] = value
  end

  def execute
    raise NotImplementedError
  end

  def status
    success? ? :ok : :unprocessable_entity
  end

  def success?
    errors.empty?
  end

  def failure?
    errors.any?
  end

  private

  attr_reader :context
end
