class Stock
  class DeleteStock < ApplicationInteractor
    def execute
      Stock.update(context[:id], deleted_at: Time.current).tap do |stock|
        errors.merge!(stock.errors)
      end
    end
  end
end
