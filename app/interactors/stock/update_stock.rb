class Stock
  class UpdateStock < CreateStock
    def execute
      Stock.update(context[:id], stock_params).tap do |stock|
        errors.merge!(stock.errors)
      end
    end
  end
end
