class Stock
  class ListActiveStocks < ApplicationInteractor
    def execute
      Stock.not_deleted.eager_load(:bearer)
    end
  end
end
