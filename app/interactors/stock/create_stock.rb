class Stock
  class CreateStock < ApplicationInteractor
    def execute
      Stock.create(stock_params).tap do |stock|
        errors.merge!(stock.errors)
      end
    end

    def status
      success? ? :created : :unprocessable_entity
    end

    private

    def stock_params
      context.slice(:name).merge(bearer_params)
    end

    def bearer_params
      return { bearer_id: context[:bearer_id] } if context.key?(:bearer_id)
      return { bearer: ensure_bearer } if context.key?(:bearer_name)

      {}
    end

    def ensure_bearer
      Bearer.where(name: context[:bearer_name]).first_or_create
    end
  end
end
