class Stock < ApplicationRecord
  belongs_to :bearer, optional: false

  validates_associated :bearer
  validates :name, presence: true,
                   uniqueness: { case_sensitive: false,
                                 conditions: -> { not_deleted } }

  scope :not_deleted, -> { where(deleted_at: nil) }

  def readonly?
    deleted_at.present? && deleted_at_was.present?
  end
end
