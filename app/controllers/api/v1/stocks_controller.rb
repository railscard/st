module API
  module V1
    class StocksController < ApplicationController
      def index
        render_outcome Stock::ListActiveStocks.call
      end

      def create
        render_outcome Stock::CreateStock.call(stock_params)
      end

      def update
        render_outcome Stock::UpdateStock.call(id: params[:id], **stock_params)
      end

      def destroy
        render_outcome Stock::DeleteStock.call(id: params[:id])
      end

      private

      def stock_params
        params.require(:stock).permit(:name, :bearer_id, :bearer_name)
      end
    end
  end
end
