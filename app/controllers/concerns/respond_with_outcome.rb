module RespondWithOutcome
  def render_outcome(outcome)
    if outcome.success?
      render json: outcome.result, status: outcome.status
    else
      render json: outcome.errors, status: :unprocessable_entity
    end
  end
end
