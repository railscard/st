class ApplicationController < ActionController::API
  include RespondWithOutcome

  rescue_from ActiveRecord::RecordNotFound,
              ActiveRecord::ReadOnlyRecord,
              ActionController::RoutingError do
    render json: { error: "Nothing to see here, it's better to check Netflix!" },
           status: :not_found
  end

  rescue_from ActionController::ParameterMissing do |e|
    render json: { error: e.message },
           status: :bad_request
  end
end
