# README

### A few notes on my implementation
- It is very possible that I haven't understood the requirements well enough. In such a case, please let me know what has been done wrong and I'll fix the code.
- In my implementation, there are two ways to assign a bearer to a stock, by providing either a bearer name or a bearer ID. A new bearer record will be created only if there are no records with the same name. There is no way to update or delete a bearer.
- The code may seem overcomplicated, that is (mostly) intentionally. I wanted to show my approach without using many gems. In the real project I would use libraries such as ActiveInteration / Interactor, Responders, dry-rb, etc.

### CURL snippets for testing
```bash
# List all active stocks
curl localhost:3000/api/v1/stocks

# Create a stock
curl -X POST http://localhost:3000/api/v1/stocks \
  -H 'Content-Type: application/json' \
  -d '{"stock": { "name": "STOCK", "bearer_name": "BEARER" } }'

# Update a stock name
curl -X PUT http://localhost:3000/api/v1/stocks/<ID> \
  -H 'Content-Type: application/json' \
  -d '{"stock": { "name": "STOCK-UPDATE" } }'

# Update a stock bearer using an ID
curl -X PUT http://localhost:3000/api/v1/stocks/<ID> \
  -H 'Content-Type: application/json' \
  -d '{"stock": { "bearer_id": <ID> } }'

# Update a stock bearer using a name
curl -X PUT http://localhost:3000/api/v1/stocks/<ID> \
  -H 'Content-Type: application/json' \
  -d '{"stock": { "name": "STOCK-UPDATE-2", "bearer_name": "BEARER-UPDATE" } }'

# Delete a stock (softly)
curl -X DELETE http://localhost:3000/api/v1/stocks/<ID>
```

Also, there is a test suite available in `test/controllers/stocks_controller_test.rb`
