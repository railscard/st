FactoryBot.define do
  factory :stock do
    sequence(:name) { |n| "STOCK#{n}" }
    bearer { create(:bearer) }
    deleted_at { nil }

    trait :deleted do
      deleted_at { Time.current }
    end
  end
end
