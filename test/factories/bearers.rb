FactoryBot.define do
  factory :bearer do
    sequence(:name) { |n| "BEARER#{n}" }
  end
end
