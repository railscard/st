require "test_helper"

class Stock::ListActiveStockTest < ActiveSupport::TestCase
  def test_execute_returns_non_deleted_stocks
    active_stock = create(:stock)
    deleted_stock = create(:stock, :deleted)

    stocks = Stock::ListActiveStocks.call.result

    assert_includes stocks, active_stock
    refute_includes stocks, deleted_stock
  end
end
