require "test_helper"

class Stock::CreateStockTest < ActiveSupport::TestCase
  def test_create_stock_with_new_bearer
    assert_difference ["Bearer.count", "Stock.count"], 1 do
      Stock::CreateStock.call(name: "STOCK 1", bearer_name: "BEARER 1")
    end
  end

  def test_create_stock_with_existing_bearer_by_id
    bearer = create(:bearer, name: "BEARER 1")

    assert_difference -> { Bearer.count } => 0, -> { Stock.count } => 1 do
      Stock::CreateStock.call(name: "STOCK 2", bearer_id: bearer.id)
    end
  end

  def test_create_stock_with_existing_bearer_by_name
    create(:bearer, name: "BEARER 1")

    assert_difference -> { Bearer.count } => 0, -> { Stock.count } => 1 do
      Stock::CreateStock.call(name: "STOCK 2", bearer_name: "BEARER 1")
    end
  end

  def test_create_stock_with_invalid_name
    outcome = Stock::CreateStock.call(name: "", bearer_name: "BEARER 1")

    assert outcome.failure?
    assert outcome.errors.added?(:name, :blank)
    refute outcome.result.persisted?
  end

  def test_create_stock_without_bearer
    outcome = Stock::CreateStock.call(name: "STOCK 3")

    assert outcome.failure?
    assert outcome.errors.key?(:bearer)
    refute outcome.result.persisted?
  end
end
