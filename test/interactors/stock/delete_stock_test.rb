require "test_helper"

class Stock::DeleteStockTest < ActiveSupport::TestCase
  def test_execute_deletes_stock_softly
    stock = create(:stock)
    outcome = Stock::DeleteStock.call(id: stock.id)

    assert outcome.success?
    assert_not_nil stock.reload.deleted_at
  end
end
