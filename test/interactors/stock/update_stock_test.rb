require "test_helper"

class Stock::UpdateStockTest < ActiveSupport::TestCase
  def test_update_stock_with_new_name
    bearer = create(:bearer, name: "BEARER 1")
    stock = create(:stock, name: "STOCK 1", bearer: bearer)

    Stock::UpdateStock.call(id: stock.id, name: "NEW-STOCK")

    assert_equal stock.reload.name, "NEW-STOCK"
  end

  def test_update_stock_with_new_bearer
    bearer = create(:bearer, name: "BEARER 1")
    stock = create(:stock, name: "STOCK 1", bearer: bearer)

    assert_difference -> { Bearer.count } => 1, -> { Stock.count } => 0 do
      Stock::UpdateStock.call(id: stock.id, bearer_name: "BEARER 2")
    end
  end
end
