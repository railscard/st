require "test_helper"

class StockTest < ActiveSupport::TestCase
  def setup
    create(:stock)
  end

  context "associations" do
    should belong_to(:bearer)
  end

  context "validations" do
    should validate_presence_of(:name)
    should validate_uniqueness_of(:name).case_insensitive
  end
end
