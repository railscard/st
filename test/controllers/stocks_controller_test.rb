require "test_helper"

module API
  module V1
    class StocksControllerTest < ActionDispatch::IntegrationTest
      def setup
        @bearer = create(:bearer, name: "BEARER")
        @deleted_stock = create(:stock, :deleted, name: "DELETED", bearer: @bearer)
        @active_stock = create(:stock, name: "ACTIVE", bearer: @bearer)
      end

      def test_index_returns_active_stocks
        get api_v1_stocks_url

        assert_response :success
        assert JSON::Validator.validate(stock_schema, response.parsed_body, list: true)
        assert_equal 1, response.parsed_body.size
        assert_equal @active_stock.name, response.parsed_body[0]["name"]
      end

      def test_create_stock
        assert_difference -> { Stock.count } do
          post api_v1_stocks_url,
               params: { stock: { name: "STOCK", bearer_name: "BEARER" } },
               as: :json
        end

        assert_response :created
        assert JSON::Validator.validate(stock_schema, response.parsed_body)
        assert_equal "STOCK", response.parsed_body["name"]
      end

      def test_create_stock_with_invalid_params
        assert_no_difference -> { Stock.count } do
          post api_v1_stocks_url,
               params: { stock: { name: "", bearer_id: 0 } },
               as: :json
        end

        assert_response :unprocessable_entity
        assert_equal(
          { "name" => ["can't be blank"], "bearer" => ["must exist"] },
          response.parsed_body
        )
      end

      def test_update_stock_name
        assert_no_difference -> { Stock.count } do
          put api_v1_stock_url(@active_stock),
              params: { stock: { name: "NEW-STOCK" } },
              as: :json
        end

        assert_response :success
        assert JSON::Validator.validate(stock_schema, response.parsed_body)
        assert_equal "NEW-STOCK", @active_stock.reload.name
      end

      def test_update_stock_bearer
        assert_difference "Bearer.count", 1 do
          put api_v1_stock_url(@active_stock),
              params: { stock: { bearer_name: "NEW-BEARER" } },
              as: :json
        end

        assert_response :success
        assert JSON::Validator.validate(stock_schema, response.parsed_body)
        assert_equal "BEARER", @bearer.reload.name
        assert_equal "NEW-BEARER", @active_stock.reload.bearer.name
      end

      def test_soft_delete_stock
        assert_no_difference "Stock.count" do
          delete api_v1_stock_url(@active_stock)

          assert_response :success
          assert_not_nil @active_stock.reload.deleted_at
        end
      end

      def test_soft_delete_already_deleted_stock
        delete api_v1_stock_url(@deleted_stock)

        assert_response :missing
      end

      private

      def stock_schema
        JSON.parse(File.read(Rails.root.join("test/schemas/stock.json")))
      end
    end
  end
end
